-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")

local gio     = require("lgi").Gio

-- local modules
local bindings   = require("bindings")
local commondefs = require("commondefs")
local utils      = require("utils")
local workspace  = require("workspace")

-- local widgets
local BatteryManager = require("battery_mng")
local pager          = require("pager")
local urgent_wgt     = require("urgent_wgt")

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Oops, there were errors during startup!",
                     text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
                         text = tostring(err) })
        in_error = false
    end)
end
-- }}}

-- {{{ Variable definitions
-- Themes define colours, icons, font and wallpapers.
beautiful.init(awful.util.getdir("config") .. "/theme.lua")

-- Table of layouts to cover with awful.layout.inc, order matters.
local layouts =
{
    awful.layout.suit.tile,
    awful.layout.suit.tile.bottom,
}
-- }}}

-- Panel

-- systray
local tray_icon_size = 24
local systray = wibox.widget.systray()

local function update_systray_iconsize()
    systray:set_base_size(beautiful.xresources.apply_dpi(tray_icon_size, screen.primary))
end

screen.connect_signal('primary_changed', update_systray_iconsize)
update_systray_iconsize()

-- widget indicating a desk with urgent clients
-- global, shown only on the primary screen
local u_wgt = urgent_wgt.UrgentWidget:new()

local b_mng = BatteryManager()

-- Create a wibox for each screen and add it
mywibox = {}
mypromptbox = {}

local wsp = workspace.Workspace:new(layouts, 24)

awful.screen.connect_for_each_screen(function(s)
    utils.log('screen', 'Setting up screen %d:', s.index)
    for k, v in pairs(s.outputs) do
        utils.log('screen', '  %s', k)
    end

    local panel_width  = beautiful.xresources.apply_dpi(128, s)

    -- Create a promptbox for each screen
    mypromptbox[s] = awful.widget.prompt({
        bg = beautiful.bg_urgent,
        fg = beautiful.fg_urgent
    })

    -- Create an imagebox widget which will contains an icon indicating which layout we're using.
    local layoutbox = awful.widget.layoutbox(s)
    layoutbox:buttons(gears.table.join(
                      awful.button({ }, 1, function () awful.layout.inc(layouts, 1) end),
                      awful.button({ }, 3, function () awful.layout.inc(layouts, -1) end),
                      awful.button({ }, 4, function () awful.layout.inc(layouts, 1) end),
                      awful.button({ }, 5, function () awful.layout.inc(layouts, -1) end)))

    local layout_utils = wibox.layout.fixed.horizontal()
    layout_utils:add(layoutbox)
    layout_utils:add(b_mng)
    layout_utils:add(u_wgt.widget)

    local utils_bar = wibox.container.constraint(layout_utils, 'max', nil, beautiful.xresources.apply_dpi(tray_icon_size))

    -- Create a tasklist widget
    local pgr = pager.Pager:new(wsp, s)

    -- clock + calendar
    local clock_time = wibox.widget.textclock("<span size=\"18000\">%H:%M</span>", 20)
    local clock_date = wibox.widget.textclock("<span size=\"11500\">%a %F</span>", 60)
    clock_time:set_align("center")
    clock_date:set_align("center")

    local calendar_month = awful.widget.calendar_popup.month({ screen = s })
    calendar_month:attach(clock_time, 'br', { on_hover = false })

    local calendar_year = awful.widget.calendar_popup.year({ screen = s })
    calendar_year:attach(clock_date, 'br', { on_hover = false })

    -- Widgets that are aligned to the bottom of the tool bar
    local layout_bottom = wibox.layout.fixed.vertical()

    layout_bottom:add(systray)

    layout_bottom:add(utils_bar)
    layout_bottom:add(clock_time)
    layout_bottom:add(clock_date)

    -- Now bring it all together (with the tasklist in the middle)
    local layout = wibox.layout.align.vertical()
    layout:set_top(mypromptbox[s])
    layout:set_middle(pgr.widget)
    layout:set_bottom(layout_bottom)

    -- Create the wibox
    mywibox[s] = awful.wibar({ position = "right", screen = s, width = panel_width })
    mywibox[s]:set_widget(layout)

    -- clean up on screen removal
    s:connect_signal("removed",
        function (s)
            mywibox[s]     = nil
            mypromptbox[s] = nil
        end)

    -- show desktop <n> on <n>th screen
    gears.timer.delayed_call(function(s) wsp:view(s, s.index, 1) end, s)
end)


clientbuttons = gears.table.join(
    awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
    awful.button({ commondefs.modkey }, 1, awful.mouse.client.move),
    awful.button({ commondefs.modkey }, 3, awful.mouse.client.resize))

local globalkeys, clientkeys = bindings.create(wsp)

-- Set keys
root.keys(globalkeys)

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = 3,
                     border_color = "#000000",
                     focus = awful.client.focus.filter,
                     raise = true,
                     keys = clientkeys,
                     buttons = clientbuttons,
                     screen = awful.screen.preferred
                    } },
    { rule = { class = "mpv" },
      properties = { floating = true } },
}
-- }}}

-- {{{ EWMH activate handling
--

-- custom workspace-aware activate handler
local function ewmh_activate(c, context, hints)
    local desk_idx = utils.tag_desk_idx(c.first_tag)
    local page_idx = utils.tag_page_idx(c.first_tag)

    if desk_idx == nil or page_idx == nil then
        utils.warn('EWMH activate', 'client on unknown desk/page')

        desk_idx = 1
        page_idx = 1

        wsp:move_client(c, desk_idx, page_idx)
    end

    if c:isvisible() then
        client.focus = c
    end

    if hints.raise and not awesome.startup and not c:isvisible() then
        c.urgent = true
    end

    wsp:view(c.screen, desk_idx, page_idx)
end

-- replace builtin activate handler by ours
client.disconnect_signal("request::activate", awful.ewmh.activate)
client.connect_signal("request::activate",    ewmh_activate)

-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c, startup)
    -- Prevent clients from being unreachable
    awful.placement.no_offscreen(c)

    if startup then
        return
    end

    -- Put windows in a smart way, only if they does not set an initial position.
    if not c.size_hints.user_position and not c.size_hints.program_position then
        awful.placement.no_overlap(c)
    end

    -- put new clients at the end
    awful.client.setslave(c)

    -- give the new client focus if it spawns on the focused page
    if c.focusable and c.first_tag == mouse.screen.selected_tag then
        -- use pcall() because the client might be gone by the time we get to it
        gears.timer.delayed_call(pcall, function() c:jump_to(false) end)
    end
end)

client.connect_signal("focus", function(c) c.border_color = "#b0e2ff" end)
client.connect_signal("unfocus", function(c) c.border_color = "#000000" end)

-- Enable sloppy focus
client.connect_signal("mouse::enter",
    function(c)
        if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier
            and awful.client.focus.filter(c) then
            client.focus = c
        end
    end)

-- }}}

-- show a notification on changing tag properties
do
    local tag_prop_notify = {
        master_count        = 'Master count',
        column_count        = 'Column count',
        master_width_factor = 'Master width factor',
    }

    for name, desc in pairs(tag_prop_notify) do
        tag.connect_signal('property::' .. name,
            function(tag)
                utils.notify_singleton(wsp, name,
                    { title = desc, text = tostring(tag[name]), timeout = 2 })
            end)
    end
end

-- show notification on display brightness changes
local function brightness_stdout(line)
    local action, device = string.match(line, '^KERNEL%[%d+.%d*%]%s+(%a+)%s+([^%s]+)%s+%(backlight%)$')
    if action ~= 'change' then
        return
    end

    local function readnum(name)
        local file = gio.File.new_for_path('/sys/' .. device .. '/' .. name)
        local contents = file:load_contents(nil)

        if contents then
            contents = string.gsub(contents, "%s$", "")
            return string.len(contents) > 0 and tonumber(contents) or nil
        end

        return nil
    end

    local brightness     = readnum('brightness')
    local max_brightness = readnum('max_brightness')

    if brightness and max_brightness then
        utils.notify_singleton(wsp, 'brightness',
                               { title = 'Brightness',
                                 text = tostring(100 * brightness // max_brightness) .. '%'})
    end

    return nil
end
local function brightness_exit(reason, code)
    utils.warn('brightness', 'udevadm monitor exited: %s/%d', reason, code)
end
awful.spawn.with_line_callback('udevadm monitor --kernel --subsystem-match=backlight',
                               { stdout = brightness_stdout, exit = brightness_exit })
