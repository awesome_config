local M = {}

local Stack = {}

function Stack:push(v)
    table.insert(self._et, v)
end

function Stack:pop()
    return table.remove(self._et)
end

function Stack:remove(it)
    local idx = nil
    for i, v in ipairs(self._et) do
        if v == it then
            idx = i
            break
        end
    end
    if idx == nil then
        error(it .. " not in stack")
    end
    table.remove(self._et, idx)
end

function Stack:new()
    local o = setmetatable({}, self)
    self.__index = self

    o._et = {}

    return o
end

M.Stack = Stack

return M
