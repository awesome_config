local M = {}

local awful      = require("awful")
local gears      = require("gears")
local timer      = require("gears.timer")

local commondefs = require("commondefs")
local utils      = require("utils")

function M.create(workspace)
    local modkey = commondefs.modkey

    local globalkeys = gears.table.join(
        awful.key({ modkey, "Mod1"    }, "Delete", awesome.restart),
        awful.key({ modkey            }, "Pause",           utils.screen_lock),
        awful.key({                   }, "XF86ScreenSaver", utils.screen_lock),

        -- emitted by ACPI handler to indicate imminent suspend
        awful.key({                   }, "XF86PowerOff",    utils.screen_lock),

        awful.key({ modkey,           }, "Insert",
            function ()
                local s = mouse.screen
                for o, _ in pairs(s.outputs) do
                    awful.spawn('randr_output_toggle ' .. o)
                end
            end),

        -- something of a hack: diziet is configured to send F13/XF86Tools on lid close
        -- this should trigger a screen lock if HDMI cable is not connected
        awful.key({                   }, "XF86Tools",
            function ()
                awful.spawn.with_shell("cat /sys/class/drm/card0/*HDMI*/status|grep -q '^connected' || " ..
                                       "{ xscreensaver-command -suspend; xset dpms force off; };")
            end),
        awful.key({                   }, "XF86Launch5",
            function ()
                awful.spawn.with_shell("xscreensaver-command -deactivate")
            end),

        -- program launching
        awful.key({ modkey,           }, "t", function () utils.spawn_current(commondefs.terminal) end),
        awful.key({ modkey,           }, "v", function () utils.spawn_current(commondefs.terminal .. " -e alsamixer") end),
        awful.key({ modkey,           }, "Escape", function () utils.spawn_current(commondefs.terminal .. " -e htop") end),

        -- audio control
        awful.key({ modkey },   "Prior",                 function () utils.vol_control(workspace,  1)  end),
        awful.key({ modkey },   "Next",                  function () utils.vol_control(workspace, -1)  end),
        awful.key({        },   "XF86AudioRaiseVolume",  function () utils.vol_control(workspace,  1)  end),
        awful.key({        },   "XF86AudioLowerVolume",  function () utils.vol_control(workspace, -1)  end),

        awful.key({ modkey },   "End",                   function () utils.vol_mute_toggle(workspace)  end),
        awful.key({        },   "XF86AudioMute",         function () utils.vol_mute_toggle(workspace)  end),

        awful.key({ modkey },   "Delete",                function () utils.auto_mute_toggle(workspace) end),

        awful.key({ modkey },   "grave",                 function () awful.spawn("mpc toggle") end),
        awful.key({        },   "XF86AudioPlay",         function () awful.spawn("mpc toggle") end),

        awful.key({ modkey },   "b",      function () awful.spawn("mpc next")   end),
        awful.key({ modkey },   "p",      function () awful.spawn("mpc prev")   end),

        -- focus/screen switching
        awful.key({ modkey,           }, "q",      function() utils.screen_focus_physical(1) end),
        awful.key({ modkey,           }, "w",      function() utils.screen_focus_physical(2) end),
        awful.key({ modkey,           }, "e",      function() utils.screen_focus_physical(3) end),

        awful.key({ modkey,           }, "Up",     function() workspace:view_relative(-1) end   ),
        awful.key({ modkey,           }, "Down",   function() workspace:view_relative(1)  end   ),
        awful.key({ modkey,           }, "k",      function() workspace:view_relative(-1) end   ),
        awful.key({ modkey,           }, "j",      function() workspace:view_relative(1)  end   ),
        awful.key({ modkey,           }, "h",      function() awful.screen.focus_bydirection("left")  end   ),
        awful.key({ modkey,           }, "l",      function() awful.screen.focus_bydirection("right") end   ),

        awful.key({ modkey,           }, "Tab",
            function ()
                awful.client.focus.byidx(1)
                if client.focus then client.focus:raise() end
            end),
        awful.key({ modkey, "Shift"   }, "Tab",
            function ()
                awful.client.focus.byidx(-1)
                if client.focus then client.focus:raise() end
            end),
        awful.key({ modkey, "Mod1"    }, "Tab",
            function ()
                awful.client.focus.history.previous()
                if client.focus then
                    client.focus:raise()
                end
            end),
        awful.key({ modkey, "Control" }, "j",
            function ()
                awful.client.focus.bydirection("down")
                if client.focus then client.focus:raise() end
            end),
        awful.key({ modkey, "Control" }, "k",
            function ()
                awful.client.focus.bydirection("up")
                if client.focus then client.focus:raise() end
            end),
        awful.key({ modkey, "Control" }, "l",
            function ()
                awful.client.focus.bydirection("right")
                if client.focus then client.focus:raise() end
            end),
        awful.key({ modkey, "Control" }, "h",
            function ()
                awful.client.focus.bydirection("left")
                if client.focus then client.focus:raise() end
            end),
        awful.key({ modkey, "Control" }, "Return",
            function ()
                local c = awful.client.getmaster()
                if c then
                    c:jump_to(false)
                    c:raise()
                end
            end),

        -- Layout manipulation
        awful.key({ modkey,           }, "Return",
            function()
                if client.focus then
                    awful.client.setmaster(client.focus)
                end
            end),

        awful.key({ modkey,           }, ",", function () awful.tag.incnmaster( 1)      end),
        awful.key({ modkey,           }, ".", function () awful.tag.incnmaster(-1)      end),
        awful.key({ modkey, "Control" }, ",", function () awful.tag.incncol( 1)         end),
        awful.key({ modkey, "Control" }, ".", function () awful.tag.incncol(-1)         end),

        awful.key({ modkey,           }, "bracketright",     function () awful.tag.incmwfact( 0.05)    end),
        awful.key({ modkey,           }, "bracketleft",      function () awful.tag.incmwfact(-0.05)    end),
        awful.key({ modkey, "Shift"   }, "bracketright",     function () awful.client.swap.byidx(  1)    end),
        awful.key({ modkey, "Shift"   }, "bracketleft",      function () awful.client.swap.byidx( -1)    end),
        awful.key({ modkey,           }, "\\",
            function () awful.layout.inc( 1, nil, workspace.layouts)          end),
        awful.key({ modkey, "Control" }, "\\",
            function () awful.layout.inc(-1, nil, workspace.layouts)          end),

        -- move to upper/lower page
        awful.key({ modkey, "Shift"   }, "j", function() workspace:shift_cur_client(1)  end),
        awful.key({ modkey, "Shift"   }, "k", function() workspace:shift_cur_client(-1) end),

        -- swap two screens
        awful.key({ modkey, "Control"   }, "q", function() workspace:swap_screens(1) end),
        awful.key({ modkey, "Control"   }, "w", function() workspace:swap_screens(2) end),
        awful.key({ modkey, "Control"   }, "e", function() workspace:swap_screens(3) end),

        -- rename focused desktop
        awful.key({ modkey }, "n",
            function()
                awful.prompt.run(
                    { prompt       = "Rename desktop: ",
                      textbox      = mypromptbox[mouse.screen].widget,
                      exe_callback =
                        function(input)
                            if input then
                                local cur_desk = workspace.screen_state[mouse.screen].desktop_idx
                                workspace:rename_desktop(cur_desk, input)
                            end
                        end})

            end),
        -- view desktop by user-entered number
        awful.key({ modkey }, "d",
            function()
                awful.prompt.run(
                    { prompt       = "Select desktop: ",
                      textbox      = mypromptbox[mouse.screen].widget,
                      exe_callback =
                        function(input)
                            if input then
                                local desktop = workspace.desktops[tonumber(input)]
                                if desktop then
                                    workspace:view(mouse.screen, desktop.idx)
                                else
                                    utils.warn('Workspace',
                                               'No such desktop: %d', input)
                                end
                            end
                        end})

            end),
        -- move focused client to desktop by user-entered number
        awful.key({ modkey, "Shift" }, "d",
            function()
                awful.prompt.run(
                    { prompt       = "Move client to desktop: ",
                      textbox      = mypromptbox[mouse.screen].widget,
                      exe_callback =
                        function(input)
                            if input and client.focus then
                                local desktop = workspace.desktops[tonumber(input)]

                                if desktop == nil then
                                    utils.warn('Workspace',
                                               'No such desktop: %d', input)
                                end

                                local page_idx = desktop:find_empty_page() or 1
                                workspace:move_client(client.focus, desktop.idx, page_idx)
                            end
                        end})

            end),

        -- launch rofi in window finder mode
        awful.key({ modkey }, "/", function() awful.spawn('rofi -show window -show-icons') end),

        awful.key({ modkey }, "'", function()
            utils.spawn_current(commondefs.terminal .. " -e ipython3",
                                { floating = true, placement = awful.placement.centered })
            end)
    )

    -- Switching between pages on current desk
    -- Be careful: we use keycodes to make it works on any keyboard layout.
    -- This should map on the top row of your keyboard, usually 1 to 9.
    for i = 1, 10 do
        globalkeys = gears.table.join(globalkeys,
            -- View page <i> on current desk
            awful.key({ modkey }, "#" .. i + 9,
                      function ()
                            local screen = mouse.screen

                            local state = workspace.screen_state[screen]
                            local desk  = 1
                            if state then
                                desk = state.desktop_idx
                            end

                            workspace:view(screen, desk, i)
                      end),

            -- Move focused client to page <i> on current desk.
            awful.key({ modkey, "Shift" }, "#" .. i + 9,
                      function ()
                          if client.focus then
                              local c = client.focus
                              local s = c.screen
                              workspace:move_client(c, workspace.screen_state[s].desktop_idx, i)
                              timer.delayed_call(awful.screen.focus, s)
                         end
                      end))
    end

    -- Switching between desktops
    for i = 1, 12 do
        local keycode
        if i <= 10 then
            -- F1--10 are codes 67-76
            keycode = 66 + i
        else
            -- F11-12 are codes 95-96
            keycode = 84 + i
        end

        globalkeys = gears.table.join(globalkeys,
            -- View auto-selected page on desktop <i>
            awful.key({ modkey }, "#" .. keycode,
                function ()
                    local screen = mouse.screen
                    workspace:view(screen, i)
                end),

            -- View empty (or first) page on desktop <i>
            awful.key({ modkey, "Control" }, "#" .. keycode,
                function ()
                    local screen = mouse.screen
                    local desk = workspace.desktops[i]
                    local page_idx = desk:find_empty_page() or 1
                    workspace:view(screen, i, page_idx)
                end),

            -- Move focused client to empty (or first, if all non-empty)
            -- page on desktop <i>
            awful.key({ modkey, "Shift" }, "#" .. keycode,
                function ()
                    if client.focus then
                        local c = client.focus
                        local s = c.screen
                        local desk = workspace.desktops[i]
                        local page_idx = desk:find_empty_page() or 1
                        workspace:move_client(c, i, page_idx)
                        timer.delayed_call(awful.screen.focus, s)
                    end
                end))
    end

    local clientkeys = gears.table.join(
        awful.key({ modkey,           }, "f",      function (c) c.fullscreen = not c.fullscreen  end),
        awful.key({ modkey, "Shift"   }, "c",      function (c) c:kill()                         end),
        awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ),
        awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end),
        awful.key({ modkey, "Shift"   }, "q",      function (c) workspace:client_to_screen(c, utils.screen_physical(1)) end),
        awful.key({ modkey, "Shift"   }, "w",      function (c) workspace:client_to_screen(c, utils.screen_physical(2)) end),
        awful.key({ modkey, "Shift"   }, "e",      function (c) workspace:client_to_screen(c, utils.screen_physical(3)) end),
        awful.key({ modkey,           }, "m",
            function (c)
                c.maximized_horizontal = not c.maximized_horizontal
                c.maximized_vertical   = not c.maximized_vertical
                c.maximized            = not c.maximized
            end),
        awful.key({ modkey, "Shift"   }, "m",
            function (c)
                c.maximized_horizontal = false
                c.maximized_vertical   = false
                c.maximized            = false
            end),
        awful.key({ modkey, "Control" }, "m",
            function (c)
                c.maximized_horizontal = true
                c.maximized_vertical   = true
                c.maximized            = true
            end)
    )

    return globalkeys, clientkeys
end

return M
